<?php

declare(strict_types=1);

namespace Drupal\pdfpreview;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\file\FileInterface;

/**
 * Generates PDF Previews.
 */
class PdfPreviewGenerator {

  /**
   * Constructs a PdfPreviewGenerator object.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly FileSystemInterface $fileSystem,
    protected readonly TransliterationInterface $transliteration,
    protected readonly ImageToolkitManager $toolkitManager,
    protected readonly LanguageManagerInterface $languageManager,
  ) {}

  /**
   * Gets the preview image if it exists, or creates it if it doesn't.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file to generate a preview for.
   */
  public function getPdfPreview(FileInterface $file): string|null {
    $destination_uri = $this->getDestinationUri($file);
    // Check if a preview already exists.
    if (file_exists($destination_uri)) {
      // Check if the existing preview is older than the file itself.
      if (filemtime($file->getFileUri()) <= filemtime($destination_uri)) {
        // The existing preview can be used, nothing to do.
        return $destination_uri;
      }
      else {
        // Delete the existing but out-of-date preview.
        $this->deletePdfPreview($file);
      }
    }
    if ($this->createPdfPreview($file, $destination_uri)) {
      return $destination_uri;
    }
    return NULL;
  }

  /**
   * Deletes the preview image for a file.
   */
  public function deletePdfPreview(FileInterface $file): void {
    $uri = $this->getDestinationUri($file);
    if (file_exists($uri)) {
      $this->fileSystem->delete($uri);
      image_path_flush($uri);
    }
  }

  /**
   * Deletes the preview image for a file when the file is updated.
   */
  public function updatePdfPreview(FileInterface $file): void {
    /** @var \Drupal\file\FileInterface $original */
    $original = $file->original ?? $file;
    if ($file->getFileUri() !== $original->getFileUri()
      || filesize($file->getFileUri()) !== filesize($original->getFileUri())) {
      $this->deletePdfPreview($original);
    }
  }

  /**
   * Creates a preview image of the first page of a PDF file.
   */
  protected function createPdfPreview(FileInterface $file, string $destination): bool {
    $local_path = $this->fileSystem->realpath($file->getFileUri());
    if ($local_path === FALSE) {
      return FALSE;
    }

    $config = $this->configFactory->get('pdfpreview.settings');

    /** @var \Drupal\imagemagick\Plugin\ImageToolkit\ImagemagickToolkit $toolkit */
    $toolkit = $this->toolkitManager->createInstance('imagemagick');

    $directory = $this->fileSystem->dirname($destination);
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $toolkit->arguments()
      ->add(['-background', 'white'])
      ->add(['-flatten'])
      ->add(['-resize', $config->get('size')])
      ->add(['-quality', (string) $config->get('quality')]);
    if ($config->get('type') == 'png') {
      $toolkit->arguments()->setDestinationFormat('PNG');
    }
    else {
      $toolkit->arguments()->setDestinationFormat('JPG');
    }
    $toolkit->arguments()->setSourceFormat('PDF');
    $toolkit->arguments()->setSourceLocalPath($local_path);
    $toolkit->arguments()->setSourceFrames('[0]');

    return $toolkit->save($destination);
  }

  /**
   * Gets the destination URI of the file.
   */
  protected function getDestinationUri(FileInterface $file): string {
    $config = $this->configFactory->get('pdfpreview.settings');
    $scheme = $this->configFactory->get('system.file')->get('default_scheme');
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $output_path = $scheme . '://' . $config->get('path');

    if ($config->get('filenames') == 'human') {
      $filename = $this->fileSystem->basename($file->getFileUri(), '.pdf');
      $filename = $this->transliteration->transliterate($filename, $langcode);
      $filename = $file->id() . '-' . $filename;
    }
    else {
      $filename = md5('pdfpreview' . $file->id());
    }

    if ($config->get('type') == 'png') {
      $extension = '.png';
    }
    else {
      $extension = '.jpg';
    }

    return $output_path . '/' . $filename . $extension;
  }

}
