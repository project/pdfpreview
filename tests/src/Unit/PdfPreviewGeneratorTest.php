<?php

declare(strict_types=1);

namespace Drupal\Tests\pdfpreview\Unit;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\file\FileInterface;
use Drupal\pdfpreview\PdfPreviewGenerator;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\pdfpreview\PdfPreviewGenerator
 *
 * @group pdfpreview
 */
class PdfPreviewGeneratorTest extends UnitTestCase {

  /**
   * @covers ::getDestinationUri
   * @dataProvider destinationUriProvider
   */
  public function testGetDestinationUri(string $filenames, string $type, string $expected): void {
    $reflection = new \ReflectionClass(PdfPreviewGenerator::class);
    $method = $reflection->getMethod('getDestinationUri');
    $method->setAccessible(TRUE);

    $config = $this->createMock(ImmutableConfig::class);
    $config
      ->method('get')
      ->willReturnMap([
        ['path', 'pdfpreview'],
        ['filenames', $filenames],
        ['type', $type],
      ]);

    $file_config = $this->createMock(ImmutableConfig::class);
    $file_config
      ->method('get')
      ->willReturn('public');

    $configFactory = $this->createMock(ConfigFactoryInterface::class);
    $configFactory
      ->method('get')
      ->willReturnMap([
        ['pdfpreview.settings', $config],
        ['system.file', $file_config],
      ]);

    $file = $this->getFileMock('Test File.pdf', 1234);
    $generator = $this->getPdfPreviewGeneratorMock($configFactory);

    $this->assertEquals($expected, $method->invoke($generator, $file));
  }

  /**
   * Data provider for testGetDestinationUri.
   */
  public function destinationUriProvider(): array {
    return [
      ['md5', 'jpg', 'public://pdfpreview/117aa294817277a5c090bf18d515d885.jpg'],
      ['md5', 'png', 'public://pdfpreview/117aa294817277a5c090bf18d515d885.png'],
      ['human', 'png', 'public://pdfpreview/1234-test-file.png'],
    ];
  }

  /**
   * Gets a mocked PDF Preview Generator for testing.
   *
   * @return \Drupal\pdfpreview\PdfPreviewGenerator
   *   Mocked PDF Preview Generator.
   */
  protected function getPdfPreviewGeneratorMock(ConfigFactoryInterface $configFactory): PdfPreviewGenerator {
    $file_system = $this->createMock('\Drupal\Core\File\FileSystem');
    $file_system
      ->expects($this->any())
      ->method('basename')
      ->with('public://Test File.pdf', '.pdf')
      ->willReturn('Test File');
    $transliteration = $this->createMock(TransliterationInterface::class);
    $transliteration
      ->expects($this->any())
      ->method('transliterate')
      ->with('Test File', 'en')
      ->willReturn('test-file');

    $image_toolkit_manager = $this->createMock('\Drupal\Core\ImageToolkit\ImageToolkitManager');

    $language = $this->createMock('Drupal\Core\Language\Language');
    $language
      ->expects($this->any())
      ->method('getId')
      ->willReturn('en');

    $language_manager = $this->createMock('Drupal\Core\Language\LanguageManager');
    $language_manager
      ->expects($this->any())
      ->method('getCurrentLanguage')
      ->willReturn($language);

    $generator = $this->getMockBuilder('\Drupal\pdfpreview\PdfPreviewGenerator')
      ->setConstructorArgs([
        $configFactory,
        $file_system,
        $transliteration,
        $image_toolkit_manager,
        $language_manager,
      ])
      ->getMock();

    return $generator;
  }

  /**
   * Gets a mocked file for testing.
   *
   * @param string $filename
   *   The filename.
   * @param int $id
   *   The file id.
   *
   * @return \Drupal\file\FileInterface
   *   The mocked file.
   */
  protected function getFileMock(string $filename, int $id): FileInterface {
    $file = $this->createMock('\Drupal\file\FileInterface');
    $file->expects($this->any())
      ->method('id')
      ->willReturn($id);
    $file->expects($this->any())
      ->method('getFileUri')
      ->willReturn('public://' . $filename);
    return $file;
  }

}
